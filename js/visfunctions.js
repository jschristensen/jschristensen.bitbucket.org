// Data: {key: komid, value: color}

function renderMap(error, div, datakom, datasogn, caption, legend) {
   // Map setup   
   var m_width =   $(div).width(),
      width    = 938
      height    = 500,
      active = d3.select(null);

    console.log(m_width);

    var drag = d3.behavior.drag()
        // .origin(function(d) { return d; })
        // .on("mousedown", dragcoordinates)
        .origin(Object)
        .on("dragstart", dragcoordinates)
        .on("drag", dragmove);

  var projection = d3.geo.mercator()
      .center([8, 56.0])
      .rotate([-5, 0])
      .scale(4000);

    var zoom = d3.behavior.zoom()
       .translate([0, 0])
       .scale(1)
       .scaleExtent([1, 30])
       .on("zoom", zoomed);

   var path = d3.geo.path()
      .projection(projection);

   // Map destination 

   var map = d3.select(div);

   // SVG

   var svg = map.append("svg")
      .attr("preserveAspectRatio", "xMidYMid")
      .attr("viewBox", "0 0 " + width + " " + height)
      .attr("width", m_width)
      .attr("height", m_width*height/width)
      // .append("g")
      ;

   // DEFS - clipping area - everything outside does not draw/render
   var defs = svg.append("defs");

   defs.append("svg:clipPath")
      .attr("id", "clip")
      .append("rect")
       .attr("x","0")
       .attr("y","0")
       .attr("width", m_width)
       .attr("height", m_width*height/width);

    // Container for our map
   var g = svg.append("g")
      .attr("clip-path", "url(#clip)")
      ;

   // Background - blue (sea)

   g.append("rect")
       .attr("class", "background")
       .attr("fill", "#73B6E6")
       .attr("width", m_width)
       .attr("height", m_width*height/width)
       .on("click", reset);

   // Array containing references to our geo-data
   var geo = {
      kommune: 'data/kommuner.json',
      sogn: 'data/dk.topo.json',
      byer: 'data/byer.json',
      motorvej: 'data/motorveje.topo.json'
   };

   var kommuner = g.append("g")
      // .attr("clip-path", "url(#clip)")
      .attr("id", "kommuner");

   var sogne = g.append("g")
      // .attr("clip-path", "url(#clip)")
      .attr("id", "sogne");

   var kommuner_click = g.append("g")
      .attr("id", "kommunerclick");

   var byer = g.append("g")
      // .attr("clip-path", "url(#clip)")
      .attr("id", "byer");

   var motorveje = g.append("g")
      // .attr("clip-path", "url(#clip)")
      .attr("id", "motorveje");

   var labels = g.append("g")
      // .attr("clip-path", "url(#clip)")
      .attr("id", "labels");   

   g.call(zoom);

   g.call(zoom.event);

   queue()
      .defer(d3.json, geo.sogn)
      .defer(d3.json, geo.kommune)
      .defer(d3.json, geo.byer)
      .defer(d3.json, geo.motorvej)
      .await(ready);


      // console.log(topo);
   function ready(error, toposogn, topokommune, topobyer, topomotorvej) {

      // Legend
            var x = d3.scale.linear()
                .domain(d3.extent(legend.scale.domain()))
                .range([0, 200]);

            formatter = d3.format("0.000%");

            var ticks = []

            for (k in d3.range(4)) { ticks.push(legend.scale.invertExtent(legend.colors[k])[0]) ;} ;
            ticks.push(d3.max(legend.scale.domain()));

            var xAxis = d3.svg.axis()
                .scale(x)
                .orient("bottom")
                .tickSize(0)
                .tickFormat(formatter)
                .tickValues(ticks)
                ;

            var g_ = svg.append("g")
                   .attr("class", "key")
                   .attr("transform", "translate(400,40)")
                   .attr("font-size", "8")
                   .attr("fill", "#fff")
                   .attr("cursor","move")
                   .call(drag);

               g_.selectAll("rect")
                   .data(legend.scale.range().map(function(d, i) {
                     return {
                       x0: i ? x(legend.scale.invertExtent(legend.colors[i])[0]) : x.range()[0],
                       x1: i < legend.scale.range().length ? x(legend.scale.invertExtent(legend.colors[i])[1]) : x.range()[1],
                       z: d
                     };
                   }))
                 .enter().append("rect")
                   .attr("height", 8)
                   .attr("y", -8)
                   .attr("x", function(d) { return d.x0; })
                   .attr("width", function(d) { return d.x1 - d.x0; })
                   .style("fill", function(d) { return d.z; });

               g_.call(xAxis)
                     .append("text")
                   .attr("class", "caption")
                   .attr("font-weight", "normal")
                   .attr("y", -10)
                   .text(caption);

      kommuner.selectAll("path")
         .data(d3.nest()
            .key(function(d) { return d.properties.KOMKODE; })
            .sortKeys(function(a,b) { return a - b ;})
            .entries(toposogn.objects.sogn.geometries))
         .enter().append("path")
         .attr("id", "kommuner")
         .attr("class", "kommuner")
         .attr("opacity", 0.8)
         // TODO: rewrite more general 
         .attr("fill", function(d) { if (!_.isUndefined(datakom[d.key.substr(1,4)])) { return datakom[d.key.substr(1,4)]["color"];} })
         .attr("d", function(d) { return path(topojson.merge(toposogn, d.values)); })
         // .on("click", clicked)
         // .append("title")
         //    .text(function(d) {return "Kommunekode: "+d.id+"\n"+
         //                "Kommune: "+d.properties['name']+"\n"+
         //                "Befolkningsudvikling: "+datakom[d.id]["diff"]
         //       })
         ;




      kommuner_click.selectAll("path")
         .data(d3.nest()
            .key(function(d) { return d.properties.KOMKODE; })
            .sortKeys(function(a,b) { return a - b ;})
            .entries(toposogn.objects.sogn.geometries))
         .enter().append("path")
         .attr("opacity",0)
         .attr("id", "kommunerclick")
         // TODO: rewrite more general 
         .attr("fill", "#000")
         .attr("d", function(d) { return path(topojson.merge(toposogn, d.values)); })
         .on("click", clicked);

      // Grænser 
      kommuner_click.append("path")
         .datum(topojson.mesh(toposogn, toposogn.objects.sogn, function(a,b) { return a === b | a.properties.KOMKODE !== b.properties.KOMKODE  ;}))
         .attr("class", "border")
         // .attr("clip-path", "url(#clip)")
         .attr("d", path)
         .attr("fill", "none")
         .attr("stroke", "#000")
         .attr("stroke-width", 0.5)
         .attr("stroke-opacity", .5);


      motorveje.append("path")
         .datum(topojson.feature(topomotorvej, topomotorvej.objects.motorveje))
         .attr("class", "veje")
         .attr("d", path)
         .attr("fill","none")
         .attr("stroke", "#000")
         .attr("stroke-width", .5)
        .attr("opacity", 0.5)
         ;

      motorveje.append("path")
         .datum(topojson.feature(topomotorvej, topomotorvej.objects.motorveje))
         .attr("class", "veje")
         .attr("d", path)
         .attr("fill","none")
         .attr("stroke", "#f33")
         .attr("stroke-width", .25)
         .attr("opacity", 0.5)
         ;

      byer.selectAll("path")
         .data(topojson.feature(topobyer, topobyer.objects.byer).features)
         .enter()
         .append("path")
         .attr("id", function(d) {return d.properties.name ;})
         .attr("class", "byer")
         .attr("fill", "red")
         .attr("stroke", "#000")
         .attr("opacity",0)
         .attr("d", path.pointRadius(1))
         ;

      labels.selectAll("text")
         .data(topojson.feature(topobyer, topobyer.objects.byer).features)
         .enter().append("text")
         .attr("opacity",0)
         .attr("class","labels")
         .attr("font-size", "1.5px")
         .attr("transform", function(d) { return "translate(" + projection(d.geometry.coordinates) + ")"; })
         .attr("dy", ".35em")
         .attr("dx", ".99em")
         .text(function(d) { return d.properties.name; });



   };



   // }

   function clicked(d) {


     if (active.node() === this) return reset();
     active.classed("active", false);
     active = d3.select(this).classed("active", true);

     d3.json('data/dk.topo.json', function(toposogn) {

         var bounds = path.bounds(topojson.merge(toposogn, d.values)),
             dx = bounds[1][0] - bounds[0][0],
             dy = bounds[1][1] - bounds[0][1],
             x = (bounds[0][0] + bounds[1][0]) / 2,
             y = (bounds[0][1] + bounds[1][1]) / 2,
             scale = .9 / Math.max(dx / width, dy / height),
             translate = [width / 2 - scale * x, height / 2 - scale * y];

         g
            .transition()
             .duration(750)
             .call(zoom.translate(translate).scale(scale).event)

           // Tegn sogne
          d3.selectAll("#sogne").selectAll("path").remove()

          d3.selectAll("#sogne").selectAll("path")
             .data(topojson.feature(toposogn, toposogn.objects.sogn).features.filter(function(e) {
                return e.properties.KOMKODE === d.key ; 
             }))
             .enter().append("path")
             .transition()
             .attr("id", "sogne")
             .attr("d", path)
             .attr("class", "sogne")
             .attr("fill", function(e) {if (!_.isUndefined(datasogn[e.properties.SOGNEKODE])) {  return datasogn[e.properties.SOGNEKODE]["color"];} })
             .attr("opacity",1)
             .attr("stroke", "#000")
             .attr("stroke-width", 0.02)
             .attr("stroke-opacity", 1)
          ;

            d3.selectAll(".byer")
            .transition()
            .duration(2000)
             .ease("linear")
          .attr("opacity",1)
           .attr("d", path.pointRadius(5/scale));

         d3.selectAll(".labels")
            .transition()
            .duration(2000)
             .ease("linear")
          .attr("opacity",1)
          .attr("font-size", 16/scale + "px");

         d3.selectAll("#kommuner")
            .transition()
            .duration(500)
             .ease("linear")
          .attr("fill", "#B0D0AB");

          });

   }

   function reset() {
     active.classed("active", false);
       active = d3.select(null);

     d3.selectAll("#sogne").selectAll("path").transition()
        .duration(200)
         .ease("linear")
        .remove();
      // .attr("opacity",0);

     d3.selectAll(".byer").transition()
        .duration(200)
         .ease("linear")
      .attr("opacity",0);

     d3.selectAll(".labels").transition()
        .duration(200)
         .ease("linear")
      .attr("opacity",0);

     d3.selectAll("#kommuner").selectAll("path").transition()
        .duration(100)
         .ease("linear")
      .attr("fill", function(d) { if (!_.isUndefined(datakom[d.key.substr(1,4)])) { return datakom[d.key.substr(1,4)]["color"];} })
;

     g.transition()
           .duration(750)
           .call(zoom.translate([0,0]).scale(1).event);
   }

function zoomed() {
  var translate = d3.event.translate ;
  var scale = d3.event.scale;

  defs.selectAll("rect")
           // .delay(delay)
             // .duration(100)
           //   .ease("linear")
             .attr("x", -translate[0] / scale)
             .attr("y", -translate[1] / scale)
             .attr("width", m_width / scale)
             .attr("height", height*(m_width/width)/ scale)
             ;
  g.selectAll("path").style("stroke-width", 1.5 / scale + "px");
  g.attr("transform", "translate(" + translate + ")scale(" + scale + ")");




}

// If the drag behavior prevents the default click,
// also stop propagation so we don’t click-to-zoom.
function stopped() {
  if (d3.event.defaultPrevented) d3.event.stopPropagation();
}

function dragcoordinates(d) {

  d3.event.sourceEvent.stopPropagation(); // silence other listeners

    x0 = d3.mouse(this)[0];
    y0 = d3.mouse(this)[1];
    // x0 = d3.event["sourceEvent"].clientX;
    // y0 = d3.event["sourceEvent"].clientY;
}

function dragmove(d) {
    var element = d3.select(this);
    var dx = d3.event.dx;
    var dy = d3.event.dy;
    var translate = d3.transform(element.attr("transform")).translate;
    tx = translate[0]+ dx;
    ty = translate[1] + dy;
    d3.select(this).attr("transform", "translate(" + tx + "," + ty + ")");
}

$(window).resize(function() {
  var w = $(div).width();

  // d3.select(div).select("svg").attr("width", w);
  // d3.select(div).select("svg").attr("height", w * height / width);
  svg.attr("width", w);
  svg.attr("height", w * height / width);

});


};